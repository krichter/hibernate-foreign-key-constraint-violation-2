package de.richtercloud.hibernate.foreign.key.constraint.violation;

import java.io.Serializable;
import javax.ejb.Local;

@Local
public interface EnterpriseBean0I extends Serializable {

    void method0(Entity1 entity1,
            Entity0 entity0);

    void method1(Entity1 entity1,
            Entity0 entity0);
}
