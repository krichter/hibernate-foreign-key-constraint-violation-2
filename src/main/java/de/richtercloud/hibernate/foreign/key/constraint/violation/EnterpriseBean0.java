package de.richtercloud.hibernate.foreign.key.constraint.violation;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class EnterpriseBean0 implements EnterpriseBean0I {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseBean0.class);
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void method0(Entity1 entity1,
            Entity0 entity0) {
        entityManager.persist(entity1);
        entityManager.persist(entity0);
    }
    
    @Override
    public void method1(Entity1 entity1,
            Entity0 entity0) {
        entityManager.remove(entityManager.merge(entity1));
        entity0.setEntity1(null);
        entityManager.remove(entityManager.merge(entity0));
    }
}
