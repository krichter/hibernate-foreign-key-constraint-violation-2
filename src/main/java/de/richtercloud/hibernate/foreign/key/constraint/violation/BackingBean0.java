package de.richtercloud.hibernate.foreign.key.constraint.violation;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class BackingBean0 implements Serializable {
    private static final long serialVersionUID = 1L;
    @EJB
    private EnterpriseBean0I enterpriseBean0;

    public String label() {
        Entity1 entity1 = new Entity1();
        Entity0 entity0 = new Entity0();
        enterpriseBean0.method0(entity1,
                entity0);
        enterpriseBean0.method1(entity1,
                entity0);
        return "Hello world!";
    }
}
